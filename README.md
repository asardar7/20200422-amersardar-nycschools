# 20200422-AmerSardar-NYCSchools

Show a list of NYC High Schools. 
A school can be selected from the list to show it's SAT scores

If there is more time then some changes I would make to my code:

1. Add check for no network
2. Show previously return api response when the app comes into the foreground and then sync for new data
3. Use SerialedName annotation for altering member names in the model classes
4. Store in db for quick access and then sync upon initial launch